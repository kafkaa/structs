"""various python implementations of stacks and queues"""
from collections import deque

class StackEmptyError(Exception):
    pass


class QueueEmptyError(Exception):
    pass


class SimpleStack:
    """classic stack using python list"""
    def __init__(self):
        self.items = []

    def __repr__(self):
        return f"{type(self).__name__}({self.items})"

    def __len__(self):
        return len(self.items)

    @property
    def empty(self):
        return self.items == []

    def peek(self):
        if self.items:
            return self.items[-1]
        raise StackEmptyError('stack is empty')

    def push(self, item):
        self.items.append(item)

    def pop(self):
        if stack.items:
            return self.items.pop()
        raise StackEmptyError('stack is empty')


class LinkedListStack:
    """stack implemented using singly liked list"""
    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None

        def __repr__(self):
            return f"{type(self).__name__}(data={self.data})"

    def __init__(self):
        self.head = None

    def __len__(self):
        return self._size(node=self.head)

    @property
    def empty(self):
        return self.head is None

    def _size(self, node=None):
        if node is not None:
            return self._size(node.next) + 1
        return 0

    def peek(self):
        if not self.empty:
            return self.head.data
        raise StackEmptyError('stack is empty')

    def push(self, item):
        node = self.Node(item)
        node.next = self.head
        self.head = node

    def pop(self):
        if not self.empty:
            item = self.head.data
            self.head = self.head.next
            return item
        raise StackEmptyError('stack is Empty')


class SimpleQueue:
    """queue using Collections.deque for O(1) constant enqueue and dequeue times"""
    def __init__(self):
        self.queue = deque()

    @property
    def empty(self):
        return self.queue == []

    def __repr__(self):
        return f"{type(self).__name__}.{str(self.queue)}"

    def __len__(self):
        return self.size

    @property
    def size(self):
        return len(self.queue)

    def enqueue(self, item):
        self.queue.append(item)

    def dequeue(self):
        return self.queue.popleft()

    def peek(self):
        return self.queue[0]


class LinkedListQueue:
    """queue implemented using doubly linked list. Constant time O(1) for enqueue and dequeue"""
    class Node:
        def __init__(self, item):
            self.item = item
            self.next = None
            self.prev = None

    def __init__(self):
        self.head = None
        self.tail = None

    def __repr__(self):
        return f"{[node.item for node in self]}"

    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.next

    def __len__(self):
        return self._size(node=self.head)

    def _size(self, node=None):
        if node is not None:
            return self._size(node.next) + 1
        return 0

    @property
    def empty(self):
        return self.head is None

    def peek(self):
        if self.tail:
            return self.tail.item
        raise QueueEmptyError('queue is empty')

    def enqueue(self, item):
        node = self.Node(item)
        if self.head:
            node.next = self.head
            self.head.prev = node
            self.head = node
            if not self.tail:
                self.tail = node
        else:
            self.head = self.tail = node

    def dequeue(self):
        if self.tail:
            item = self.tail.item
            self.tail = self.tail.prev
            if self.tail:
                self.tail.next = None
            else:
                self.head = self.tail = None

            return item
        raise QueueEmptyError('no items in the queue')
