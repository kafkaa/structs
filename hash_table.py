"""python implementation of hash table"""
from hashlib import blake2b

class Bucket:
    class Node:
        def __init__(self, key, val):
            self.key = key
            self.value = val
            self.next = None

        def __repr__(self):
            return f'{type(self).__name__}(key={self.key}, value={self.value})'

    def __init__(self):
        self.head = None

    def empty(self):
        return self.head == None

    def push(self, key, value):
        node = self.Node(key, value)
        node.next = self.head
        self.head = node

    def find(self, key):
        prev = None
        node = self.head
        while node and node.key != key:
            prev = node
            node = node.next

        return prev, node

    def delete(self, previous, node):
        if previous:
            previous.next = node.next

        elif node is self.head and node.next:
           self.head = node.next

        else:
            self.head = None

        del node

class HashTable:
    def __init__(self, capacity):
        self.cap = capacity
        self.map = [Bucket() for index in range(self.cap)]

    def __setitem__(self, key, value):
        self.insert(key, value)

    def __getitem__(self, key):
        value = self.fetch(key)
        if value:
            return value
        raise KeyError(key)

    @property
    def is_full(self):
        """returns true if over half-capacity"""
        result = 0
        for index in self.map:
            if not index.empty:
                result += 1

        return result > len(self.map) // 2

    def _add_slots(self):
        """doubles the size of the table"""
        self.map.extend([Bucket() for index in range(self.cap)])
        self.cap *= 2

    def hashsum(self, key):
        return int(blake2b(key.encode()).hexdigest(), 16) % len(self.map)

    def insert(self, key, value):
        if self.is_full:
            self._add_slots()

        bucket = self.map[self.hashsum(key)]
        if bucket.head:
            _, node = bucket.find(key)
            if node:
                node.value = value
            else:
                bucket.push(key, value)
        else:
            bucket.push(key, value)

    def remove(self, key):
        bucket = self.map[self.hashsum(key)]
        if bucket.head:
            bucket.delete(*bucket.find(key))
        else:
            raise KeyError(key)

    def fetch(self, key, default=None):
        index = self.hashsum(key)
        bucket = self.map[index]
        _,  node = bucket.find(key)
        if node:
            return node.value
        return default
