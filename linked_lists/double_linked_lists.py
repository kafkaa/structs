"""python implementation of ordered and unordered double linked lists"""
class DoubleLinkedUnordered:
    class Node:
        def __init__(self, item):
            self.item = item
            self.next = None
            self.prev = None

        def set(self, item):
            self.item = item

        def get_next(self):
            return self.next

        def set_next(self, node):
            self.next = node

        def get_previous(self):
            return self.prev

        def set_previous(self, node):
            self.prev = node

    def __init__(self):
        self.head = None
        self.tail = None

    def __repr__(self):
        return f'{type(self).__name__}({[node.item for node in self]})'

    def __len__(self):
        return self._size(node=self.head)

    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.get_next()

    def __contains__(self, item):
        for node in self:
            if node.data == item:
                return True
        return False

    def __getitem__(self, index):
        return self._index(index, sliced=DoubleLinkedUnordered(), node=self.head)

    @property
    def empty(self):
        return self.head is None

    def _size(self, node=None):
        if node:
            return self._size(node.get_next()) + 1
        return 0

    def _index(self, index, sliced=None, node=None, i=0):
        if isinstance(index, slice):
            start = 0 if index.start is None else index.start
            stop = len(self) if index.stop is None else index.stop
            while i <= stop and node:
                if start <= i < stop:
                    sliced.append(node.item)
                node = node.get_next()
                i += 1
            return sliced
        else:
            if node:
                if i == index:
                    return node.item
                return self._index(index, node=node.get_next(), i=i + 1)
            raise IndexError('index out of range')

    def find(self, item):
        for index, node in self:
            if node.item == item:
                return index, item
        raise ValueError(f"{item} is not in list")

    def prepend(self, item):
        node = self.Node(item)
        if self.head:
            node.set_next(self.head)
            self.head.set_previous(node)
            self.head = node
        else:
            self.head = self.tail = node

    def append(self, item):
        node = self.Node(item)
        if self.tail:
            self.tail.set_next(node)
            node.set_previous(self.tail)
            self.tail = node
        else:
            self.head = self.tail = node

    def pop(self, index=0):
        for enum, node in enumerate(self):
            if enum == index:
                next_node = node.get_next()
                prev_node = node.get_previous()

                if next_node is None:
                    self.tail = next_node
                    self.tail.set_next(None)

                elif prev_node is None:
                    self.head = next_node
                    self.head.set_previous(None)

                else:
                    prev_node.set_next(next_node)

                return node.item

        raise IndexError('index out of range')

    def pop_tail(self):
        item = self.tail.item
        self.tail = self.tail.get_previous()
        self.tail.set_next(None)
        return item

    def delete(self, index):
        for enum, node in enumerate(self):
            if enum == index:
                next_node = node.get_next()
                prev_node = node.get_previous()

                if next_node is None:
                    self.tail = prev_node
                    self.tail.set_next(None)

                elif prev_node is None:
                    self.head = next_node
                    self.head.set_previous(None)

                else:
                    prev_node.set_next(next_node())

        raise IndexError("index out of range")

    def rotate(self, left=True):
        if left:
            self.append(self.pop())
        else:
            self.prepend(self.pop_tail())

    def reverse(self):
        next_node, prev_node = None, None
        node, self.tail = self.head, self.head

        while node:
            next_node, node.next = node.next, prev_node
            prev_node, node = node, next_node

        self.head = prev_node

class DoubleLinkedOrdered(DoubleLinkedUnordered):

    def __getitem__(self, index):
        return self._index(index, sliced=DoubleLinkedOrdered(), node=self.head)

    def insert(self, item):
        if item <= self.head.item:
            self.prepend(item)

        elif item >= self.tail.item:
            self.append(item)

        else:
            for node in self:
                next_node = node.get_next()
                if node.item <= item <= next_node.item:
                    insertion = self.Node(item)
                    insertion.set_previous(node)
                    insertion.set_next(node.get_next())
                    node.set_next(insertion)
                    break


