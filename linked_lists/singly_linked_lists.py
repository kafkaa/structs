"""python implementation of ordered and unordered singly linked listss"""
class SinglyLinkedUnordered:
    class Node:
        def __init__(self, data):
            self.data = data
            self.next = None

        def __repr__(self):
            return str(self.data)

        def set(self, data):
            self.data = data

        def get_next(self):
            return self.next

        def set_next(self, node):
            self.next = node

    def __init__(self):
        self.head = None
        self.tail = None

    def __repr__(self):
        return f"{type(self).__name__}({[node.data for node in self]})"

    def __len__(self):
        return self._size(node=self.head)

    def __iter__(self):
        node = self.head
        while node:
            yield node
            node = node.get_next()

    def __contains__(self, item):
        for node in self:
            if node.data == item:
                return True
        return False

    def __getitem__(self, index):
        return self._index(index, sliced=SinglyLinkedUnordered(), node=self.head)

    @property
    def empty(self):
        return self.head is None

    def _size(self, node=None):
        if node:
            return self._size(node.get_next()) + 1
        return 0

    def _index(self, index, sliced=None, node=None, i=0):
        if isinstance(index, slice):
            start = 0 if index.start is None else index.start
            stop = len(self) if index.stop is None else index.stop
            while i <= stop and node is not None:
                if start <= i < stop:
                    sliced.append(node.data)
                node = node.get_next()
                i += 1
            return sliced
        else:
            if node is not None:
                if i == index:
                    return node.data
                return self._index(index, node=node.get_next(), i=i + 1)
            raise IndexError('index out of range')

    def _find(self, item, node=None, index=0):
        if node:
            if node.data == item:
                return index, node.data
            return self._fetch(item, node=node.get_next(), i=i + 1)
        raise ValueError(f'{item} is not in list')

    def _pop(self, index, previous=None, node=None, i=0):
        if node:
            if index:
                if i == index:
                    if previous:
                        next_node = node.get_next()
                        previous.set_next(next_node)
                        if next_node is None:
                            self.tail = previous
                    else:
                        self.head = previous
                    return node.data
                return self._pop(index, previous=node, node=node.get_next(), i=i + 1)

            self.head = node.get_next()
            return node.data
        raise IndexError('index out of range')

    def _remove(self, item, previous=None, node=None, i=0):
        if node:
            if node.data == item:
                if previous:
                    next_node = node.get_next()
                    previous.set_next(next_node)
                    if next_node is None:
                        self.tail = previous
                else:
                    self.head = previous
                return
            return self._remove(item, previous=node, node=node.get_next(), i=i + 1)
        raise ValueError(f'{item} not in list')

    def append(self, item):
        node = self.Node(item)
        if self.tail is None:
            self.prepend(item)
        else:
            self.tail.set_next(node)
            self.tail = node

    def prepend(self, item):
        node = self.Node(item)
        node.set_next(self.head)
        self.head = node
        if self.tail is None:
            self.tail = node

    def pop(self, index):
        return self._pop(index, node=self.head)

    def delete(self, item):
        return self._remove(item, node=self.head)

    def find(self, item):
        return self._fetch(item, node=self.head)


class SinglyLinkedOrdered(SinglyLinkedUnordered):

    def __getitem__(self, index):
        return self._index(index, sliced=SinglyLinkedOrdered(), node=self.head)

    def insert(self, item):
        if item <= self.head.data:
            self.prepend(item)

        elif item >= self.tail.data:
            self.append(item)

        else:
            previous = self.head
            for node in self:
                if node.data >= item:
                    new_node = self.Node(item)
                    new_node.set_next(next_node)
                    previous.set_next(new_node)
                    break

                previous = node
                next_node = node.get_next()
